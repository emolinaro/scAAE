# scAAE
Single cell RNA seq analysis with Adversarial Autoencoders

## Settings
* Programmming language: Python 3.7
* Deep learning framework: [Keras](https://keras.io/)
