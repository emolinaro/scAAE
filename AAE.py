from keras.layers import Lambda, Input, Dense, BatchNormalization, Dropout
from keras.models import Model
from keras.losses import mse, binary_crossentropy
from keras.optimizers import Adam, SGD
from keras.initializers import RandomNormal
from keras.utils import plot_model
from keras import backend as K
from keras.callbacks import TensorBoard

import numpy as np
import os

from utils import sampling


#kernel_initializer =  RandomNormal(mean=0.0, stddev=0.01, seed=None)
#bias_initializer= RandomNormal(mean=0.0, stddev=0.01, seed=None)

kernel_initializer='glorot_uniform'
bias_initializer='zeros'


def build_encoder(original_dim, latent_dim, layer_1_dim, layer_2_dim):
    
    # TODO: implement: 1) DETERMINISTIC POSTERIOR Q(z|x); 2) UNIVERSAL APPROXIMATOR POSTERIOR

    # GAUSSIAN POSTERIOR
    
    encoder_input = Input(shape=(original_dim, ), name="X")
    x = Dropout(rate=0.2, name='DO')(encoder_input)
    x = Dense(layer_1_dim, activation='relu', name="H1", 
              kernel_initializer=kernel_initializer, 
              bias_initializer=bias_initializer)(x)
    x = BatchNormalization(name='BN_1')(x)
    x = Dense(layer_2_dim, activation='relu', name="H2", 
              kernel_initializer=kernel_initializer, 
              bias_initializer=bias_initializer)(x)
    x = BatchNormalization(name='BN_2')(x)
    z_mean = Dense(latent_dim, name='z_mean', 
                   kernel_initializer=kernel_initializer, 
                   bias_initializer=bias_initializer)(x)
    z_log_var = Dense(latent_dim, name='z_log_var', 
                      kernel_initializer=kernel_initializer, 
                      bias_initializer=bias_initializer)(x)
    z = Lambda(sampling, output_shape=(latent_dim,), name='Z')([z_mean, z_log_var])
    z = BatchNormalization(name='BN_3')(z)

    # instantiate encoder model
    encoder = Model(encoder_input, [z_mean, z_log_var, z], name='encoder')
    
    return encoder


def build_decoder(original_dim, latent_dim, layer_1_dim, layer_2_dim):
    
    decoder_input = Input(shape=(latent_dim,), name='Z')
    x = Dense(layer_1_dim, activation='relu', name="H1", 
              kernel_initializer=kernel_initializer, 
              bias_initializer=bias_initializer)(decoder_input)
    x = Dense(layer_2_dim, activation='relu', name="H2", 
              kernel_initializer=kernel_initializer, 
              bias_initializer=bias_initializer)(x)
    x = Dense(original_dim, activation='sigmoid', name="Xp", 
              kernel_initializer=kernel_initializer, 
              bias_initializer=bias_initializer)(x)

    # instantiate decoder model
    decoder = Model(decoder_input, x, name='decoder')
    
    return decoder

def build_discriminator(latent_dim, layer_1_dim, layer_2_dim):
    
    
    # build discriminator model

    discr_input = Input(shape=(latent_dim,), name='Z')
    x = Dense(layer_1_dim, activation='relu', name="H1", 
              kernel_initializer=kernel_initializer, 
              bias_initializer=bias_initializer)(discr_input)
    x = Dense(layer_2_dim, activation='relu', name="H2", 
              kernel_initializer=kernel_initializer, 
              bias_initializer=bias_initializer)(x)
    x = Dense(1, activation='sigmoid', name="Check", 
              kernel_initializer=kernel_initializer, 
              bias_initializer=bias_initializer)(x)

    # instantiate decoder model
    discriminator = Model(discr_input, x, name='discriminator')
    
    return discriminator

def build_AAE(original_dim, latent_dim, layer_1_dim, layer_2_dim):
    
    input_encoder = Input(shape=(original_dim, ), name='X')
    
    # build encoder
    encoder = build_encoder(original_dim, latent_dim, layer_1_dim, layer_2_dim)
    
    # build decoder
    decoder = build_decoder(original_dim, latent_dim, layer_1_dim, layer_2_dim)
    
    # build discriminator
    discriminator = build_discriminator(latent_dim, layer_1_dim, layer_2_dim)
    
    # instantiate AAE model
    real_input = input_encoder
    compression = encoder(real_input)[2]
    reconstruction = decoder(compression)
    generation = discriminator(compression)
 
    aae = Model(real_input, [reconstruction, generation], name='autoencoder')
    
    optimizer = Adam(0.0001, 0.5) 
    #optimizer = SGD(lr=0.01, decay=1e-6, momentum=0.9)

    discriminator.trainable = False
    discriminator.compile(optimizer=optimizer, loss="binary_crossentropy", metrics=['accuracy'])

    aae.compile(optimizer=optimizer, 
                loss=['mse', 'binary_crossentropy'],
                loss_weights=[0.9, 0.1])
    
    return encoder, decoder, discriminator, aae

def train_AAE(aae, discriminator, encoder, x_train, batch_size, latent_dim, epochs, val_split=0.0):
    
    for epoch in range(epochs):
        np.random.shuffle(x_train)
    
        for i in range(int(len(x_train) / batch_size)):
        
            batch = x_train[i*batch_size:i*batch_size+batch_size]
        
            # Regularization phase
            fake_pred = encoder.predict(batch)[2]
            real_pred = np.random.normal(size=(batch_size,latent_dim)) # prior distribution
            discriminator_batch_x = np.concatenate([fake_pred, real_pred])
            discriminator_batch_y = np.concatenate([np.zeros(batch_size), np.ones(batch_size)])
            discriminator_history = discriminator.fit(x=discriminator_batch_x, 
                                                      y=discriminator_batch_y, 
                                                      epochs=1, 
                                                      batch_size=batch_size, 
                                                      validation_split=val_split, 
                                                      verbose=0)
        
            # Reconstruction phase
            #aae.train_on_batch(batch, [batch,np.ones(batch_size,1)])
            aae_history = aae.fit(x=batch, 
                                  y=[batch, np.ones(batch_size)], 
                                  epochs=1, 
                                  batch_size=batch_size, 
                                  validation_split=val_split, 
                                  verbose=0)
    
    
        print("Epoch {0:d}/{1:d}, reconstruction loss: {2:.6f}, discriminator loss: {3:.6f}".format(
                  *[epoch+1, epochs, aae_history.history["loss"][0], discriminator_history.history["loss"][0]]))
    
    
    return discriminator_history, aae_history

    
    