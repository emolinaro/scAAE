#!/usr/bin/env python
# coding: utf-8

# # Clustering 3K PBMCs with Adversarial Autoencoders

# <img src="adversarial_autoencoder_model.png" width="800px" style="float:center" >

# In[1]:


from keras.utils.vis_utils import model_to_dot
from keras.utils import normalize
from IPython.display import SVG

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

import numpy as np
import matplotlib.pyplot as plt
import scanpy as sc
import os

from utils import plot_results
from AAE2 import *


# ## Import dataset

# In[2]:


get_ipython().system('mkdir data')
get_ipython().system('wget http://cf.10xgenomics.com/samples/cell-exp/3.0.0/pbmc_10k_protein_v3/pbmc_10k_protein_v3_filtered_feature_bc_matrix.tar.gz -O data/pbmc_10k_protein_v3_filtered_feature_bc_matrix.tar.gz')
get_ipython().system('cd data; tar -xzf pbmc_10k_protein_v3_filtered_feature_bc_matrix.tar.gz')


# ## Data preprocessing

# In[3]:


sc.settings.verbosity = 3             # verbosity: errors (0), warnings (1), info (2), hints (3)
sc.logging.print_versions()
results_file = './write/pbmc10k.h5ad'


# In[4]:


sc.settings.set_figure_params(dpi=80)


# In[5]:


adata = sc.read_10x_mtx(
   './data/filtered_feature_bc_matrix',  # the directory with the `.mtx` file
   var_names='gene_symbols',                  # use gene symbols for the variable names (variables-axis index)
   cache=True)                                # write a cache file for faster subsequent reading


# In[6]:


adata.var_names_make_unique()  # this is unnecessary if using 'gene_ids'


# In[7]:


# Show those genes that yield the highest fraction of counts in each single cells, across all cells.
sc.pl.highest_expr_genes(adata, n_top=20)

# Basic filtering
sc.pp.filter_cells(adata, min_genes=200)
sc.pp.filter_genes(adata, min_cells=3)

adata = adata[adata.obs['n_genes'] < 2500, :]


# In[8]:


# Data in log scale
sc.pp.log1p(adata)


# In[9]:


adata.obs.shape


# In[10]:


adata.obs.head()


# In[11]:


adata.var.shape


# In[12]:


adata.var.head()


# In[13]:


adata.var_names


# In[14]:


gene_names = adata.var_names.values


# In[15]:


data = adata.X.toarray()
#data = normalize(data, axis=-1, order=2)
print(np.any(np.isnan(data)))


# In[16]:


scaler = StandardScaler()
scaler.fit(data)
data = scaler.transform(data)


# ## Adversarial Autoencoder Model

# ### Network parameters

# In[17]:


original_dim = data.shape[1]

layer_1_dim = 100
layer_2_dim = 100
batch_size = 128
latent_dim = 50
epochs = 200


# In[18]:


encoder, decoder, discriminator, generator, aae = build_AAE(original_dim, latent_dim, layer_1_dim, layer_2_dim)


# In[19]:


print("\nEncoder Network")
print("===============")
encoder.summary()
os.makedirs('img', exist_ok=True)
plot_model(encoder, to_file='img/aae_encoder.png', show_shapes=True)
SVG(model_to_dot(encoder).create(prog='dot', format='svg'))


# In[20]:


print("\nDecoder Network")
print("===============")
decoder.summary()
plot_model(decoder, to_file='img/aae_decoder.png', show_shapes=True)
SVG(model_to_dot(decoder).create(prog='dot', format='svg'))


# In[21]:


print("\nAutoencoder Network")
print("===================")
aae.summary()
plot_model(aae, to_file='img/aae_autoencoder.png', show_shapes=True)
SVG(model_to_dot(aae).create(prog='dot', format='svg'))


# In[22]:


print("\nGenerator Network")
print("===================")
generator.summary()
plot_model(generator, to_file='img/aae_generator.png', show_shapes=True)
SVG(model_to_dot(generator).create(prog='dot', format='svg'))


# In[ ]:


print("\nDiscriminator Network")
print("=====================")
discriminator.summary()
plot_model(discriminator, to_file='img/aae_discriminator.png', show_shapes=True)
SVG(model_to_dot(discriminator).create(prog='dot', format='svg'))


# ### Model training

# In[ ]:


# graph=True to show the data oints in the 2-D latent space

gene = 'CST3' # used only for training visualization 

rec_loss, gen_loss, disc_loss = train_AAE(aae, generator, discriminator, encoder, decoder,
                                          data, batch_size, latent_dim, epochs, 
                                          gene, gene_names, graph=True, 
                                          val_split=0.0)

# save models
os.makedirs('models', exist_ok=True)
aae.save('models/aae_autoencoder.h5')
generator.save('models/aae_generator.h5')
encoder.save('models/aae_encoder.h5')
decoder.save('models/aae_decoder.h5')
discriminator.save('models/aee_discriminator.h5') 


# In[ ]:


# Plot training & validation loss values

plt.figure(figsize=(8, 6))
axes = plt.gca()
plt.plot(rec_loss)
plt.plot(gen_loss)
plt.plot(disc_loss)
#axes.set_xlim([xmin,xmax])
#axes.set_ylim([570,700])
#plt.loglog()
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Autoencoder', 'Generator', 'Discriminator'], loc='upper right')
plt.show()


# In[ ]:


models = (encoder, decoder)
gene_list = ['CST3', 'MALAT1', 'NKG7']
plot_results_pca(models, data, gene_list, gene_names, latent_dim)


# pca = PCA(n_components=2500)
# pca.fit(data)
# 
# plt.plot(np.cumsum(pca.explained_variance_ratio_))
# plt.xlabel('Number of components')
# plt.ylabel('Cumulative explained variance')

# In[ ]:




